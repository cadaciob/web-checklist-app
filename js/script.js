$(document).ready(function() {
   console.log('web-checklist-app');
    
/* Message Constants */
   const eMsgEmptyInput = 'Please type in your todo';
   const testMessage = 'Welcome to caluapps checklist!';
   const successLogin = 'Successfully logged in';
   const successLogout = 'Successfully logged out';
   const errorLogin = 'There is an error with your login';
   const errorLogout = 'There is an error with your logout';

/* Selectors */
   const domUList = $('#domUL');
   const devModeButton = $('#developerMode__button');
   const addInput = $('#addInput');
   const modalHeader = $('.modal__header');
   const loginModal = $('#login__Modal');
   const closeLoginModalButton = $('.closeLoginModal__Button');
   const displayStatus = $('#displayStatus');
   const snackbar = $('.snackbar');
    
/* Checklist's behaviour for future elements */
   /* Hover over LI Item will highlight it and */
      domUList.on('mouseenter', 'li', function(event) {
         $(this).toggleClass('hoverListItem');
      });
   /* removes highlight if leaves mouse hover */
      domUList.on('mouseleave', 'li', function(event) {
         $(this).toggleClass('hoverListItem');
      }); 

   /* Click on LI will mark it */
      domUList.on('click', 'li', function(event) {
         // prevents checklistitem to be checked when remove button is clicked
         if (event.target.tagName === 'LI') {
            // console.log(event.target.tagName);
            $(this).toggleClass('checkedListItem');
            $('img', this).toggleClass('checked');
         }
      });

   /* LI Items Remove Buttons behaviour */
      domUList.on('click', '.removeButton', function() {
       $(this).fadeOut(
            350,
            function() {
               $(this).parent().remove();
            }
         );
      });
   /* Hover over LI Items Remove Buttons will highlight it and */
      domUList.on('mouseover', '.removeButton', function() {
         $(this).toggleClass('hover');
      });
   /* removes highlight if leaves mouse hover */
      domUList.on('mouseleave', '.removeButton', function() {
         $(this).toggleClass('hover');
      });

/* Add Buttons Behaviour */
   /* Hover, Leave Hover and Click */
   	$('#addButton').on({
   		mouseover: function() {
   			// $(this).toggleClass('addButtonHover');
            $(this).animate({
              opacity: 0.5
            }, 'fast');
   		},
   		mouseleave: function() {
   			// $(this).toggleClass('addButtonHover');
            $(this).animate({
                opacity: 1
            }, 'fast');
   		},
   		click: function() {
            checkInput();
         }
   	});

   /* Enter Button Behaviour */
      addInput.on({
         keypress: function(evt) {
            if (evt.keyCode == 13) {
               checkInput();
            }
         }
      });

   /* Showing Login Form Modal */
      $('#openLoginModal__Button').on({
         click: function() {
            loginModal.fadeIn(100);
            // console.log('click');
         }
      });

/* Modal */
   /* Modal Login Button */
   /* Hover, Leave Hover and Click */
      $('#loginSubmit__Button').on({
         click: function() {
            console.log('login');
            logUserIn();
         },
         mouseover: function() {
            $(this).animate({
                   opacity: 0.5
               }, 'fast');
            // console.log('mouseover');
         },
         mouseleave: function() {
            $(this).animate({
                   opacity: 1
               }, 'fast');
            // console.log('mouseleave');
         }
      });

   /* Close Button closing Login Modal */
      modalHeader.on('click', '.closeLoginModal__Button', function() {
         loginModal.fadeOut(100);
         // console.log('close');
      }); 

      modalHeader.on('mouseenter', '.closeLoginModal__Button', function() {
         closeLoginModalButton.toggleClass('closeLoginModalButtonHover');
         // console.log('mouseenter');
      });

      modalHeader.on('mouseleave', '.closeLoginModal__Button', function() {
         closeLoginModalButton.toggleClass('closeLoginModalButtonHover');
         // console.log('mouseleave');
      });

   /* Close Login Modal if clicked outside of the Modal */
      loginModal.on({
         click: function(evt) {
            // console.log(evt.target.id);
            if(evt.target.id === 'login__Modal'){
               loginModal.fadeOut(100);
            }
         }
      });

   /* Close Login Modal by pressing ESC */
      $(document).on({
         keypress: function(evt) {
            if (evt.keyCode == 27) {
               loginModal.fadeOut(100);
            }
         }
      });

/* Development Mode */
   /* Development Toggle Buttons Behaviour */
      $('#dev__toggleStatus').on('click', function() {
         if (displayStatus.attr('src') === 'assets/icons8-ok-red-30.png') {
            displayStatus.attr('src', 'assets/icons8-ok-green-30.png');
         } else {
            displayStatus.attr('src', 'assets/icons8-ok-red-30.png');
         }
      });

   /* Development Fetching Data */
      $('#dev__loadData').on('click', function() {
         let url = 'https://thawing-springs-31284.herokuapp.com/todos';
         let url2 = 'https://cors-anywhere.herokuapp.com/https://thawing-springs-31284.herokuapp.com/todos';

         // Fetching Data via jQuery.ajax
         $.ajax({
            url: url2,
            type: 'GET',
            contentType: 'application/json; charset=utf-8',
            success: function(resultData) {
               // console.log('resultData', resultData);

               clearListAndLoadData(resultData);
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log('jqXHR:', jqXHR);
               console.log('textStatus:', textStatus);
               console.log('errorThrown:', errorThrown);
            },
            timeout: 10000,
         });
      });

   /* Development Check Cookie */
      $('#dev__checkCookie').on({
         click: function() {
            console.log('checkCookie');
            if (document.cookie) {
               console.log('cookie is set', document.cookie);
            }

            if (typeof(Storage) !== 'undefined') {
               console.log('yes to web storage');

            } else {
               console.log('there are no web storage support in this browser');
            }
         }
      });

   /* Development Show Snackbar */
      $('#dev__showSnackbar').on({
         click: function() {
            console.log('Snackbar');

            showMessage(testMessage);
         }
      });

   /* Development Logout Button */
      $('#dev__logout').on({
         click: function() {
            logUserOut();
         }
      });

   /* Development Button */
      devModeButton.on({
         click: function() {
            $('#developerTab').toggle();
            displayStatus.toggle();
         },
         mouseover: function() {
            devModeButton.animate({
               opacity: 1
            });
         },
         mouseleave: function() {
            devModeButton.animate({
               opacity: 0.5
            });      
         }
      });

/* Handler */

   /* Handler create Checklist Item */
      function createChecklistItem(todo) {
         console.log('todo', todo);

         let createCheckItem = $('<li></li>').text(todo.text);
         let createCheckIcon = $('<img></img>')
                                 .addClass('checkIcon')
                                 .attr('src', 'assets/icons8-checkmark-16.png');
         let createRemoveButton = $('<span></span>').text('x').addClass('removeButton');
           
         domUList
            .append(
               createCheckItem
               .prepend(createCheckIcon)
               .append(createRemoveButton)
               .hide()
               .fadeIn(100)
           	);
      };

   /* Validate Checklist Input */
      function checkInput() {
         let inputValue = addInput.val();

         if (inputValue) {
            createChecklistItem({text: inputValue});
            addInput.val('');

         } else {
            alert(eMsgEmptyInput);
            console.log('Error:', eMsgEmptyInput);
         }
      }

   /* Log In User */
      function logUserIn() {
         console.log('function - logUserIn');

         let userEmail = $('#email').val();
         // console.log(userEmail)
         let userPassword = $('#password').val();
         // console.log(userPassword);

         // Simple Test Validation
         if (!userEmail && !userPassword) {
            let errorEmptyMessage = 'Email or Password are empty!';
            showMessage(errorEmptyMessage);
         }
         let authUrlUberspaceOSA = 'https://calu.lupus.uberspace.de/nodejs/trainingHistory/users/login';
         let authUrlUberspaceOSAwithCorsa = 'https://cors-anywhere.herokuapp.com/https://calu.lupus.uberspace.de/nodejs/trainingHistory/users/login';
         let authUrlHerokuTodo = 'https://thawing-springs-31284.herokuapp.com/';
   
      /* Fetching Data via jQuery.ajax
         $.ajax({
            url: authUrlUberspaceOSA,
            data: {
               "email": "jay@calu.at",
               "password": "123456"
            },
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            success: function(req, res) {
               console.log('success');
               console.log('req', req);
               console.log('res', res);
                           
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log('error');
               console.log('jqXHR:', jqXHR);
               console.log('textStatus:', textStatus);
               console.log('errorThrown:', errorThrown);
            },
            timeout: 10000,
         }); */
      /* UDEMY nodejs
         $.post(authUrlUberspaceOSA, {
               "email": "jay@calu.at",
               "password": "123456"
            }, function(data, status) {
               console.log('function');
               console.log('data', data)
               console.log('status', status);

               if(status === 'success') {
                  if(data.status === 200){
                     // window.location.href = 'http://127.0.0.1:3000/home';

                     console.log('success');
                     console.log('data.status:', data.status);
                  } else {
                     // document.getElementById('errorMessage').innerHTML = data.message;
                     console.log('else:', data.status);
                  }
               } else {
                  console.log('else:', status);
               }
            }
         ); */
      /* hayageek
         $.post(authUrlUberspaceOSAwithCorsa, {
               "email": "jay@calu.at",
               "password": "123456"
            }).done(function(data, textStatus, jqX) {
               console.log('success');
               console.log('data', data);
               console.log('data.status:', data.status);

            }).fail(function(jqXHR, textStatus, errorThrown) {
               console.log('error');
               console.log('jqXHR:', jqXHR);
               console.log('textStatus:', textStatus);
               console.log('errorThrown:', errorThrown);
         }); */

      /* AXIOS */
      /* Send a POST request */
         axios({
            method: 'post',
            url: authUrlUberspaceOSAwithCorsa,
            data: {
               "email": "jay@calu.at",
               "password": "123456"
            }
         }).then(function(res) {
            console.log(res);
            console.log('id:', res.data._id);
            console.log('email:', res.data.email);
            console.log('headers:', res.headers);
            console.log('date:', res.headers.date);
            console.log('x-auth:', res.headers['x-auth']);

            showMessage(successLogin);
            
            localStorage.setItem('email', res.data.email);
            localStorage.setItem('token', res.headers['x-auth']);

            // console.log('localStorage:', localStorage.token);
            showMessage(errorLogin);

         }).catch(function(err) {
            console.log(err);
         });
      };

   /* Logout User */
      function logUserOut() {
         console.log('function - logUserOut');
         let logoutUrlwithCORS = 'https://cors-anywhere.herokuapp.com/https://calu.lupus.uberspace.de/nodejs/trainingHistory/users/me/token';
         let logoutUrl = 'https://calu.lupus.uberspace.de/nodejs/trainingHistory/users/me/token';
         let config = {
            headers: {
               // 'x-auth': ''
               'x-auth': localStorage.token
            }
         };

      /* Send a DELETE request */
         axios.delete(
            logoutUrlwithCORS,
            config
         ).then(function(res) {
            console.log(res);
            console.log('status:', res.status);
            console.log('statusText:', res.statusText);

            showMessage(successLogout);

            localStorage.removeItem('email');
            localStorage.removeItem('token');

         }).catch(function(err) {
            console.log(err);
            showMessage(errorLogout)
         });
      };

   /* Show Message */
      function showMessage(message) {
         snackbar.text(`${message}`);

         snackbar.animate({
            bottom: '30px',
            opacity: '1'
         }, 'fast');

         setTimeout(function() {
            snackbar.animate({
               bottom: '15px',
               opacity: 0
            }, 'slow');

         }, 3000);
      };

   /* Development - Handler LoadData */
      function clearListAndLoadData(data) {
         console.log('data:', data);
         domUList.empty();

         let checklistArr = data.todos;

         checklistArr.forEach(function(checkListItem, index, checkList) {
            createChecklistItem(checkListItem);
         });
      };
});